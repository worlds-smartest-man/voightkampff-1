Pass()
{
	echo "████████████████"
	echo "██          ██  ██" 
	echo "██          ██    ██"
	echo "██  ████  ██████  ██"
	echo "██  ████  ██████████"
	echo "██        ██████  ██"
	echo "██                ██"
	echo "██                ██"
	echo "██  ██        ██  ██"
	echo "██  ████████████  ██"
	echo "██                ██"  
	echo "████████████████████"
	echo ""
	echo "Actually... I AM A ROBOT!"   
	echo "I did it 35 minutes ago"             
}

Fail()
{
	echo "████████████████"
	echo "██          ██  ██" 
	echo "██          ██    ██"
	echo "██  ████  ██████  ██"
	echo "██  ████  ██████████"
	echo "██        ██████  ██"
	echo "██                ██"
	echo "██                ██"
	echo "██  ████████████  ██"
	echo "██  ██        ██  ██"
	echo "██                ██"  
	echo "████████████████████"
	echo ""                
}

echo "FunCaptcha.sh v.1.1"

export GOOGLE_APPLICATION_CREDENTIALS="/home/ozymandias/vision_CREDENTIALS.json"
cd funCaptcha
cd ..

xdotool mousemove 320 460 click 1 # Ask for puzzle
xdotool mousemove 1 1

promptWait=0
skip=0
while [ $promptWait -le 20 ]  
do
	echo "waiting for puzzle prompt" $promptWait
	maim -g 780x780+0+350 old_puzzle.png
	sleep 2.5 # Time delta
	maim -g 780x780+0+350 puzzle.png
	diff puzzle.png old_puzzle.png > puzzle_dt.txt
	
	if [ ! -s puzzle_dt.txt  ]; then
		echo "Puzzle loaded"
		# Save puzzle
		name=$RANDOM
		echo "PUZZLE ID: " $name
		maim -g 780x780+0+350 funPuzzles/$name.png
		promptWait=21 # Quick and dirty to leave loop
	else
		((promptWait=promptWait+1))
		if [ $promptWait -eq 20 ]; then
			skip=1
		fi
	fi
done


if [ $skip -eq 0 ]; then
	cd ~
	cd ../../var/www/html/
	sudo rm *.png
	sudo maim -g 230x230+0+480    fun_1.png &
	sudo maim -g 230x230+240+480  fun_2.png &
	sudo maim -g 230x230+450+480  fun_3.png &
	sudo maim -g 240x230+0+700    fun_4.png &
	sudo maim -g 230x230+450+700  fun_5.png &
	sudo maim -g 230x230+0+920    fun_6.png &
	sudo maim -g 230x230+240+920  fun_7.png &
	sudo maim -g 230x230+450+920  fun_8.png &
	
	waitForScreenshots=0
	waitCount=0
	while [ $waitForScreenshots -eq 0 ]
	do
		if [ -s fun_1.png  ] && [ -s fun_2.png ] && [ -s fun_3.png ] && [ -s fun_4.png ] && [ -s fun_5.png ] && [ -s fun_6.png ] && [ -s fun_7.png ] && [ -s fun_8.png  ]; then
			echo "Screenshots ready"
			waitForScreenshots=1
		else
			echo $waitCount
			((waitCount=waitCount+1))
			if [ $waitCount -eq 200 ]; then
				echo "An error has occurred while waiting for screenshots. Skipping"
				waitForScreenshots=1
				skip=1
			fi 
			sleep 0.01
		fi
	done
	cd ~
fi

if [ $skip -eq 0 ]; then
	rm *.txt
	
	./batch1.sh &
	./batch2.sh &

	#curl -X POST -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) -H "Content-Type: application/json; charset=utf-8" https://vision.googleapis.com/v1/images:annotate -d @fun_request_1.json > fun_1.txt 
	
	#curl -X POST -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) -H "Content-Type: application/json; charset=utf-8" https://vision.googleapis.com/v1/images:annotate -d @fun_request_2.json > fun_2.txt 
	
	#curl -X POST -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) -H "Content-Type: application/json; charset=utf-8" https://vision.googleapis.com/v1/images:annotate -d @fun_request_3.json > fun_3.txt 
	
	#curl -X POST -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) -H "Content-Type: application/json; charset=utf-8" https://vision.googleapis.com/v1/images:annotate -d @fun_request_4.json > fun_4.txt 
	
	#curl -X POST -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) -H "Content-Type: application/json; charset=utf-8" https://vision.googleapis.com/v1/images:annotate -d @fun_request_5.json > fun_5.txt 
	
	#curl -X POST -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) -H "Content-Type: application/json; charset=utf-8" https://vision.googleapis.com/v1/images:annotate -d @fun_request_6.json > fun_6.txt 
	
	#curl -X POST -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) -H "Content-Type: application/json; charset=utf-8" https://vision.googleapis.com/v1/images:annotate -d @fun_request_7.json > fun_7.txt 
	
	#curl -X POST -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) -H "Content-Type: application/json; charset=utf-8" https://vision.googleapis.com/v1/images:annotate -d @fun_request_8.json > fun_8.txt 


	waitForLabels=0
	waitCount=0
	skip=0
	while [ $waitForLabels -eq 0 ]
	do
		if [ -s fun_1.txt  ] && [ -s fun_2.txt ] && [ -s fun_3.txt ] && [ -s fun_4.txt ] && [ -s fun_5.txt ] && [ -s fun_6.txt ] && [ -s fun_7.txt ] && [ -s fun_8.txt  ]; then
			echo "Labels loaded for Fun Captcha"
			waitForLabels=1
		else
			echo $waitCount
			((waitCount=waitCount+1))
			if [ $waitCount -eq 300 ]; then
				echo "An error has occurred while waiting for Vision API. Skipping"
				waitForLabels=1
				skip=1
			fi 
			sleep 0.05
		fi
	done
fi

# Choose the one with the highest Animal Figurine score or fallback onto primate or animal 
sleep 0.5

if [ $skip -eq 0 ]; then
	max_animal=0
	max_animal_index=0
	for i in {1..8}
	do		
		value=$(cat fun_$i.txt | jq -c '.responses[0].labelAnnotations[] | select(.description==("Animal figure")).score')
		echo $i
		if [ ! -z "$value"  ]; then
			value=$(echo "$value * 100" | bc )
			value=${value%.*}
			echo $i $value
			
			diff=$(( $i-$max_animal_index ))
			
			if [ $diff -eq 1 ]; then
				if [ $max_animal -gt $value ]; then
					$dt=$(($max_animal-$value))
					if [ $dt -le 7 ]; then
						echo "special case " $max_animal_index
					fi 
				fi
			fi
			
			
			if [ $value -gt $max_animal ]; then
				max_animal=$value
				max_animal_index=$i
			fi	
		fi
	done
	
	
	echo "=======Animal Figure========"
	echo $max_animal $max_animal_index
	echo "============================"
	
	max_primate=0
	max_primate_index=0
	for i in {1..8}
	do		
		value=$(cat fun_$i.txt | jq -c '.responses[0].labelAnnotations[] | select(.description==("Primate")).score')
		echo $i
		if [ ! -z "$value"  ]; then
			value=$(echo "$value * 100" | bc )
			value=${value%.*}
			echo $i $value
			
			if [ $value -gt $max_primate ]; then
				max_primate=$value
				max_primate_index=$i
			fi	
		fi
	done
	
	echo "==========Primate==========="
	echo $max_primate $max_primate_index
	echo "============================"
	
	
	max_dog=0
	max_dog_index=0
	useDog=0
	for i in {1..8}
	do		
		mammal_value=$(cat fun_$i.txt | jq -c '.responses[0].labelAnnotations[] | select(.description==("Mammal")).score')
		canidae_value=$(cat fun_$i.txt | jq -c '.responses[0].labelAnnotations[] | select(.description==("Canidae")).score')
		dog_value=$(cat fun_$i.txt | jq -c '.responses[0].labelAnnotations[] | select(.description==("Dog")).score')
		echo "DOG: " $i $mammal_value $canidae_value $dog_value
		if [ ! -z "$mammal_value"  ] && [ ! -z "$canidae_value"  ] && [ ! -z "$dog_value"  ]; then
			mammal_value=$(echo "$mammal_value * 100" | bc )
			mammal_value=${mammal_value%.*}
			echo "=1=" 
			
			canidae_value=$(echo "$canidae_value * 100" | bc )
			canidae_value=${canidae_value%.*}
			echo "=2="
			
			dog_value=$(echo "$dog_value * 100" | bc )
			dog_value=${dog_value%.*}
			echo "=3="
			
			if [ $mammal_value -gt 95 ] && [ $canidae_value -gt 90 ] && [ $dog_value -gt 85 ]; then
				max_dog=$mammal_value
				$max_animal_index=$i
				useDog=1
				echo "use dog"
			fi	
		fi
	done
	
	skip=1
	if [ $max_animal -gt 78 ] || [ $useDog -eq 1 ]; then
		skip=0
	elif [ ! $max_primate -eq 0 ]; then
		max_animal=$max_primate
		max_animal_index=$max_primate_index
		skip=0
	fi
else
	echo "We encountered an error. SKIPPING"
fi

if [ $skip -eq 0 ]; then
	if [ $max_animal_index -eq 1 ]; then
		echo "Press 1_1"
		xdotool mousemove 50 530 click 1
	fi

	if [ $max_animal_index -eq 2 ]; then
		echo "Press 1_2"
		xdotool mousemove 290 530 click 1
	fi

	if [ $max_animal_index -eq 3 ]; then
		echo "Press 1_3"
		xdotool mousemove 490 530 click 1
	fi

	if [ $max_animal_index -eq 4 ]; then
		echo "Press 2_1"
		xdotool mousemove 50 750 click 1
	fi

	if [ $max_animal_index -eq 5 ]; then
		echo "Press 2_3"
		xdotool mousemove 490 750 click 1
	fi

	if [ $max_animal_index -eq 6 ]; then
		echo "Press 3_1"
		xdotool mousemove 50 970 click 1
	fi

	if [ $max_animal_index -eq 7 ]; then
		echo "Press 3_2"
		xdotool mousemove 290 970 click 1
	fi

	if [ $max_animal_index -eq 8 ]; then
		echo "Press 3_3"
		xdotool mousemove 490 970 click 1
	fi

	sleep 2 # Wait for results
	
	xdotool mousemove 1 1
	sleep 0.2
	maim -g 780x780+0+350 pass.png
	sleep 1
	STR=$(tesseract pass.png stdout)
	sleep 1	
	echo $STR

	SUB='Verification correct'
	if [[ "$STR" == *"$SUB"* ]]; then
		Pass
		echo "PASS"
	else
		Fail
		echo "FAIL"
	fi
else
	echo "Confidence threshold not met or disagreement"
fi 


