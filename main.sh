Pass()
{
	echo "████████████████"
	echo "██          ██  ██" 
	echo "██          ██    ██"
	echo "██  ████  ██████  ██"
	echo "██  ████  ██████████"
	echo "██        ██████  ██"
	echo "██                ██"
	echo "██                ██"
	echo "██  ██        ██  ██"
	echo "██  ████████████  ██"
	echo "██                ██"  
	echo "████████████████████"
	echo ""
	echo "Actually... I AM A ROBOT!"   
	echo "I did it 35 minutes ago"             
}

Fail()
{
	echo "████████████████"
	echo "██          ██  ██" 
	echo "██          ██    ██"
	echo "██  ████  ██████  ██"
	echo "██  ████  ██████████"
	echo "██        ██████  ██"
	echo "██                ██"
	echo "██                ██"
	echo "██  ████████████  ██"
	echo "██  ██        ██  ██"
	echo "██                ██"  
	echo "████████████████████"
	echo ""                
}

#. ./verifyPass_hackerNews.sh
#. ./verifyPass_reddit.sh
#. ./verifyPass_disqus.sh
. ./verifyPass_SO.sh


echo ">>> MAIN.SH V1.01"
prompt=0
attempts=$3
puzzleFail=0
Puzzle="none"
puzzleType=0
failed=0
crosswalk=0
skip=0

rm *.txt # later we check for empty files to see if Rekognition has returned results

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                                         #
#    These are state transitions the program will follow as it recurses looking for       #
#    a case it is likely to solve.                                                        #
#                                                                       -Ozymandias       #
#                                                                                         #
#    	Good = likely to solve                                                            #
#    	Bad  = probably can't solve                                                       #                
#                                                                                         #
#    V.3 Score >= 7 (click refresh ONCE, ignore cars)                                     #
#	    3x3(bad)------>4x4------------4x4                                                 #
#	    4x4------------4x4                                                                #
#	    3x3(bad)------>3x3(good/bad)                                                      #
#                                                                                         #
#    V.3 Score <=3 (CLick refresh TWICE, ignore 4x4, ignore cars, ignore buses)           #
#	    4x4(bad)------>3x3(good)                                                          #
#	    4x4(bad)------>3x3(bad)------>3x3(good/bad)                                       #
#	    3x3(bad)------>3x3(good)                                                          #
#	    3x3(bad)------>3x3(bad)------>3x3(good/bad)                                       #
#	    4x4(bad)------>4x4(bad)------>3x3(good/bad)                                       #
#	    4x4(bad)------>4x4(bad)------>4x4(bad)                                            #
#                                                                                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


promptWait=0
while [ $promptWait -le 20 ]  
do
	echo "waiting for puzzle prompt" $promptWait
	maim -g 780x245+$((245+$1))+$((270+$2)) old_prompt.png
	sleep 0.2 # Time delta, or dt
	maim -g 865x1110+$((250+$1))+$((270+$2)) puzzle.png
	maim -g 780x245+$((245+$1))+$((270+$2)) notabot/prompt.png
	diff notabot/prompt.png old_prompt.png > prompt_dt.txt
	if [ ! -s prompt_dt.txt ]; then
		echo "Puzzle prompt loaded"
		promptWait=21 # Quick and dirty to leave loop
	else
		((promptWait=promptWait+1))
	fi
done

convert -resize 200% notabot/prompt.png bigger.png
convert bigger.png -separate gray.png
raw=$(tesseract notabot/prompt.png stdout)	
STR=${raw,,} #convert to lowercase
echo $STR

SUB='squares'
if [[ "$STR" == *"$SUB"* ]]; then
	if [ $4 -le 3 ]; then
		if [ $3 -le 2 ]; then
	  	  	echo "V3 Score is too low for 4x4. Asking for another puzzle"
	  		xdotool mousemove $((300+$1)) $((1430+$2)) click 1 #235 1520
	  		sleep 2
	  		./main.sh $1 $2 $(($3+1)) $4
		fi
	  	skip=1
    else
		maim -g 865x865+$((250+$1))+$((525+$2)) notabot/image.png 
  		echo "CASE::squares"
  		puzzleType=2
	fi
fi

SUB='all images'
if [[ "$STR" == *"$SUB"* ]]; then
	maim -g 290x290+$((250+$1))+$((525+$2)) notabot/q_1_1.png &
	maim -g 290x290+$((530+$1))+$((525+$2)) notabot/q_1_2.png &
	maim -g 290x290+$((830+$1))+$((525+$2)) notabot/q_1_3.png &
	maim -g 290x290+$((250+$1))+$((815+$2)) notabot/q_2_1.png &
	maim -g 290x290+$((530+$1))+$((815+$2)) notabot/q_2_2.png &
	maim -g 290x290+$((830+$1))+$((815+$2)) notabot/q_2_3.png &
	maim -g 290x290+$((250+$1))+$((1100+$2)) notabot/q_3_1.png &
	maim -g 290x290+$((530+$1))+$((1100+$2)) notabot/q_3_2.png &
	maim -g 290x290+$((830+$1))+$((1100+$2)) notabot/q_3_3.png &
	
	waitForScreenshots=0
	waitCount=0
	while [ $waitForScreenshots -eq 0 ]
	do
		if [ -s notabot/q_1_1.png  ] && [ -s notabot/q_1_2.png ] && [ -s notabot/q_1_3.png ] && [ -s notabot/q_2_1.png ] && [ -s notabot/q_2_2.png ] && [ -s notabot/q_2_3.png ] && [ -s notabot/q_3_1.png ] && [ -s notabot/q_3_2.png ] && [ -s notabot/q_3_3.png ]; then
			echo "Screenshots ready"
			waitForScreenshots=1
		else
			echo $waitCount
			((waitCount=waitCount+1))
			if [ $waitCount -eq 200 ]; then
				echo "An error has occurred while waiting for screenshots. Skipping"
				waitForScreenshots=1
				skip=1
			fi 
			sleep 0.01
		fi
	done	
	
	SUB='none left'
	if [[ "$STR" == *"$SUB"* ]]; then
	  	echo "CASE::3x3 noneleft"
	  	puzzleType=3
	else
	  	echo "CASE::3x3 simple" 
	  	puzzleType=1
	fi
fi

if [ $skip -eq 0 ]; then

	SUB='motorcycle'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::motorcycle"
	  Puzzle=('"Name": "Motorcycle"')
	  string=("Motorcycle")
	  prompt=1
	fi

	SUB='tree'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::tree"
	  Puzzle=('"Name": "Tree"')
	  string=("Tree")
	  prompt=1
	fi

	SUB='hydrant'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::Hydrant"
	  Puzzle=('"Name": "Hydrant"' '"Name": "Fire Hydrant"')
	  string=("Hydrant" "Fire Hydrant")
	  prompt=1
	fi

	SUB='palm tree'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::palm tree"
	  Puzzle=('"Name": "Palm Tree"')
	  string=("Palm Tree")
	  prompt=1
	fi

	SUB='traffic light'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::traffic light"
	  Puzzle=('"Name": "Traffic Light"' '"Name": "Light"')
	  string=("Traffic Light" "Light")
	  prompt=1
	fi

	# if we got here on a 4x4, then the V3 score was above 3
	SUB='crosswalk'
	if [[ "$STR" == *"$SUB"* ]]; then
		SUB='squares'
		if [[ ! "$STR" == *"$SUB"* ]]; then
		  	echo "PUZZLE::zebra 3x3"
		  	Puzzle=('"Name": "Zebra Crossing"' )
		  	string=("Zebra Crossing")
		  	prompt=1
		else
		  	Puzzle=('"Name": "Zebra Crossing"' )
		  	string=("Zebra Crossing")
			prompt=1
			crosswalk=1
		
			# First column
			maim -g 324x324+$((250+$1))+$((515+$2)) notabot/w_1_1.png &
			maim -g 324x432+$((250+$1))+$((623+$2)) notabot/w_2_1.png &
			maim -g 324x432+$((250+$1))+$((839+$2)) notabot/w_3_1.png &
			maim -g 324x324+$((250+$1))+$((1055+$2)) notabot/w_4_1.png &

			# Second column
			maim -g 432x324+$((358+$1))+$((515+$2)) notabot/w_1_2.png &
			maim -g 432x432+$((358+$1))+$((623+$2)) notabot/w_2_2.png &
			maim -g 432x432+$((358+$1))+$((839+$2)) notabot/w_3_2.png &
			maim -g 432x324+$((358+$1))+$((1055+$2)) notabot/w_4_2.png &

			# Third column
			maim -g 432x324+$((582+$1))+$((515+$2)) notabot/w_1_3.png &
			maim -g 432x432+$((582+$1))+$((623+$2)) notabot/w_2_3.png &
			maim -g 432x432+$((582+$1))+$((839+$2)) notabot/w_3_3.png &
			maim -g 432x324+$((582+$1))+$((1055+$2)) notabot/w_4_3.png &

			# Fourth column
			maim -g 324x324+$((800+$1))+$((515+$2)) notabot/w_1_4.png &
			maim -g 324x432+$((800+$1))+$((623+$2)) notabot/w_2_4.png &
			maim -g 324x432+$((800+$1))+$((839+$2)) notabot/w_3_4.png &
			maim -g 324x324+$((800+$1))+$((1055+$2)) notabot/w_4_4.png &

			cd notabot
			echo "UPLOAD TIME for Crosswalks:: "
			time aws s3 sync . s3://voightkampff 
			cd ..
	
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_1_1.png"}}' > 1_1.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_2_1.png"}}' > 2_1.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_3_1.png"}}' > 3_1.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_4_1.png"}}' > 4_1.txt &

			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_1_2.png"}}' > 1_2.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_2_2.png"}}' > 2_2.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_3_2.png"}}' > 3_2.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_4_2.png"}}' > 4_2.txt &

			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_1_3.png"}}' > 1_3.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_2_3.png"}}' > 2_3.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_3_3.png"}}' > 3_3.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_4_3.png"}}' > 4_3.txt &

			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_1_4.png"}}' > 1_4.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_2_4.png"}}' > 2_4.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_3_4.png"}}' > 3_4.txt &
			aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_4_4.png"}}' > 4_4.txt &
		fi
	fi

	SUB='zebra'
	if [[ "$STR" == *"$SUB"* ]]; then
		SUB='squares'
		if [[ ! "$STR" == *"$SUB"* ]]; then
		  	echo "PUZZLE::zebra 3x3"
		  	Puzzle=('"Name": "Zebra Crossing"' )
		  	string=("Zebra Crossing")
		  	prompt=1
		fi
	fi

	SUB='bus'
	if [[ "$STR" == *"$SUB"* ]]; then
		SUB='squares'
		if [[ "$STR" == *"$SUB"* ]]; then
		  	echo "PUZZLE::bus 4x4"
		  	Puzzle=('"Name": "Bus"' '"Name": "Truck"')
		  	string=("Bus" "Truck")
		  	prompt=1
		else
			echo "PUZZLE::bus 3x3"
			if [ $4 -le 3 ]; then
			  	Puzzle=('"Name": "Bus"' '"Name": "Truck"')
			  	string=("Bus" "Truck")
				echo "Google always blurs these images when V3 score is low, skipping"
				prompt=-1
			else
			  	Puzzle=('"Name": "Bus"' '"Name": "Truck"')
			  	string=("Bus" "Truck")
				echo "Rekognition sucks with these... kipping"
			  	prompt=-1
			fi
	  	
	    fi
	fi

	SUB='parking meter'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::parking meter"
	  Puzzle=('"Name": "Parking Meter"')
	  string=("Parking Meter")
	  prompt=1
	fi

	SUB='boat'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::boat"
	  Puzzle=('"Name": "Boat"')
	  string=("Boat")
	  prompt=1
	fi

	SUB='bicycles'
	if [[ "$STR" == *"$SUB"* ]]; then
		SUB='squares'
		if [[ "$STR" == *"$SUB"* ]]; then
		  	echo "PUZZLE::Bikes 4x4"
  		    Puzzle=('"Name": "Bicycle"' '"Name": "Bike"' '"Name": "Cyclist"')
  		    string=("Bicycle" "Bike" "Cyclist")
		  	prompt=1
		else
		    echo "PUZZLE::bike 3x3"
  		    Puzzle=('"Name": "Bicycle"' '"Name": "Bike"' '"Name": "Cyclist"')
  		    string=("Bicycle" "Bike" "Cyclist")
		  	prompt=1
	    fi
	fi

	SUB='vehicles'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::vehicle"
	  Puzzle=('"Name": "Vehicle"' '"Name": "Car"' '"Name": "Bus"')
	  string=("Vehicle" "Car" "Automobile" "Bus" "Truck")
	  prompt=1
	fi

	SUB='tractor'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::tractor"
	  Puzzle=('"Name": "Tractor"')
	  string=("Tractor")
	  prompt=1
	fi

	SUB='car'
	if [[ "$STR" == *"$SUB"* ]]; then
		SUB='squares'
		if [[ "$STR" == *"$SUB"* ]]; then
		  	echo "PUZZLE::car 4x4"
		  	Puzzle=('"Name": "Car"' '"Name": "Automobile"' '"Name": "Vehicle"')
		  	string=("Car" "Automobile" "Vehicle")
		  	prompt=1
		else
			SUB='left'
			if [[ "$STR" == *"$SUB"* ]]; then
				echo "PUZZLE::car 3x3 noneleft"
			  	Puzzle=('"Name": "Car"' '"Name": "Automobile"' '"Name": "Vehicle"')
			  	string=("Car" "Automobile" "Vehicle")
				echo "Google always blurs these images, skipping"
			  	prompt=-1
			else
				echo "PUZZLE::car 3x3 simple"
			  	Puzzle=('"Name": "Car"' '"Name": "Automobile"' '"Name": "Vehicle"')
			  	string=("Car" "Automobile" "Vehicle")
				echo "Google always blurs these images, skipping"
			  	prompt=-1
			fi
	    fi
	fi

	SUB='chimney'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::chimney"
	  Puzzle=('"Name": "Chimney"')
	  string=("Chimney")
	  echo "Rekognition sucks at regognizing these. Asking for another puzzle"
	  prompt=-1
	fi

	SUB='bridge'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::bridges"
	  Puzzle=('"Name": "Bridge"')
	  string=("Bridge")
	  echo "Bridges can't be recognized. Asking for another puzzle"
	  prompt=-1
	fi

	SUB='stairs'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::stairs"
	  Puzzle=('"Name": "Stair"')
	  string=("Stair")
	  echo "can't recognize stairs"
	  prompt=-1
	fi

	SUB='statue'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::statue"
	  Puzzle=('"Name": "Monument"' '"Name": "Sculpture"' '"Name": "Statue"')
	  string=("Monument" "Sculpture" "Statue")
	  prompt=1
	fi

	SUB='taxi'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::taxis"
	  Puzzle=('"Name": "Taxi"' '"Name": "Cab"')
	  string=("Taxi" "Cab")
	  prompt=1
	fi

	SUB='mountains'
	if [[ "$STR" == *"$SUB"* ]]; then
	  echo "PUZZLE::Mountains"
	  Puzzle=('"Name": "Mountain"')
	  string=("Mountain")
	  echo "mountains::Not supported"
	  prompt=-1
	fi	

	# We know the puzzle type but still uncertain about text in the prompt(car, bus, hydrant, etc) 
	if [ ! $puzzleType -eq 0 ]; then
		./savePuzzle.sh $1 $2 &
	
		cd notabot
		echo "UPLOAD TIME:: "
		time aws s3 sync . s3://voightkampff
		cd ..
	
		aws rekognition detect-text   --image '{"S3Object":{"Bucket":"voightkampff","Name":"prompt.png"}}' > text.txt &
		if [ $puzzleType -eq 2 ]; then
		 	aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"image.png"}}'  > image.txt &
		fi

		if [ $puzzleType -eq 3 ] || [ $puzzleType -eq 1 ]; then
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_1_1.png"}}'  > 1_1.txt &
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_1_2.png"}}'  > 1_2.txt &
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_1_3.png"}}'  > 1_3.txt &
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_2_1.png"}}'  > 2_1.txt &
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_2_2.png"}}'  > 2_2.txt &
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_2_3.png"}}'  > 2_3.txt &
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_3_1.png"}}'  > 3_1.txt &
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_3_2.png"}}'  > 3_2.txt &
		    aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_3_3.png"}}'  > 3_3.txt &	
		fi

		# Wait for rekognition to find prompt text if Tesseract failed to find a valid prompt
		waitForLabels=0
		waitCount=0
		if [ $prompt -eq 0 ]; then
			while [ $waitForLabels -eq 0 ]
			do			
				# Text detection from Rekognition to get prompt has finished 
				if [ -s text.txt ]; then
					echo "Text returned from rekognition::"
					waitForLabels=1
					prompt=0

					# Process response from rekognition
					STR=`cat text.txt`

					SUB='motorcycle'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::motorcycle"
					  Puzzle=('"Name": "Motorcycle"')
					  string=("Motorcycle")
					  prompt=1
					fi

					SUB='tree'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::tree"
					  Puzzle=('"Name": "Tree"')
					  string=("Tree")
					  prompt=1
					fi

					SUB='hydrant'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::Hydrant"
					  Puzzle=('"Name": "Hydrant"' '"Name": "Fire Hydrant"')
					  string=("Hydrant" "Fire Hydrant")
					  prompt=1
					fi

					SUB='palm tree'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::palm tree"
					  Puzzle=('"Name": "Palm Tree"')
					  string=("Palm Tree")
					  prompt=1
					fi

					SUB='traffic light'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::traffic light"
					  Puzzle=('"Name": "Traffic Light"' '"Name": "Light"')
					  string=("Traffic Light" "Light")
					  prompt=1
					fi

					# if we got here on a 4x4, then the V3 score was above 3
					SUB='crosswalk'
					if [[ "$STR" == *"$SUB"* ]]; then
						SUB='squares'
						if [[ ! "$STR" == *"$SUB"* ]]; then
						  	echo "PUZZLE::zebra 3x3"
						  	Puzzle=('"Name": "Zebra Crossing"' )
						  	string=("Zebra Crossing")
						  	prompt=1
						else
						  	Puzzle=('"Name": "Zebra Crossing"' )
						  	string=("Zebra Crossing")
							prompt=1
							crosswalk=1
					
							# First column
							maim -g 324x324+$((250+$1))+$((515+$2)) notabot/w_1_1.png &
							maim -g 324x432+$((250+$1))+$((623+$2)) notabot/w_2_1.png &
							maim -g 324x432+$((250+$1))+$((839+$2)) notabot/w_3_1.png &
							maim -g 324x324+$((250+$1))+$((1055+$2)) notabot/w_4_1.png &

							# Second column
							maim -g 432x324+$((358+$1))+$((515+$2)) notabot/w_1_2.png &
							maim -g 432x432+$((358+$1))+$((623+$2)) notabot/w_2_2.png &
							maim -g 432x432+$((358+$1))+$((839+$2)) notabot/w_3_2.png &
							maim -g 432x324+$((358+$1))+$((1055+$2)) notabot/w_4_2.png &

							# Third column
							maim -g 432x324+$((582+$1))+$((515+$2)) notabot/w_1_3.png &
							maim -g 432x432+$((582+$1))+$((623+$2)) notabot/w_2_3.png &
							maim -g 432x432+$((582+$1))+$((839+$2)) notabot/w_3_3.png &
							maim -g 432x324+$((582+$1))+$((1055+$2)) notabot/w_4_3.png &

							# Fourth column
							maim -g 324x324+$((800+$1))+$((515+$2)) notabot/w_1_4.png &
							maim -g 324x432+$((800+$1))+$((623+$2)) notabot/w_2_4.png &
							maim -g 324x432+$((800+$1))+$((839+$2)) notabot/w_3_4.png &
							maim -g 324x324+$((800+$1))+$((1055+$2)) notabot/w_4_4.png &
	
							cd notabot
							echo "UPLOAD TIME for Crosswalks:: "
							time aws s3 sync . s3://voightkampff
							cd ..
				
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_1_1.png"}}' > 1_1.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_2_1.png"}}' > 2_1.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_3_1.png"}}' > 3_1.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_4_1.png"}}' > 4_1.txt &

							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_1_2.png"}}' > 1_2.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_2_2.png"}}' > 2_2.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_3_2.png"}}' > 3_2.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_4_2.png"}}' > 4_2.txt &

							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_1_3.png"}}' > 1_3.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_2_3.png"}}' > 2_3.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_3_3.png"}}' > 3_3.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_4_3.png"}}' > 4_3.txt &

							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_1_4.png"}}' > 1_4.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_2_4.png"}}' > 2_4.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_3_4.png"}}' > 3_4.txt &
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"w_4_4.png"}}' > 4_4.txt &
						fi
					fi

					SUB='zebra'
					if [[ "$STR" == *"$SUB"* ]]; then
						SUB='squares'
						if [[ ! "$STR" == *"$SUB"* ]]; then
						  	echo "PUZZLE::zebra 3x3"
						  	Puzzle=('"Name": "Zebra Crossing"' )
						  	string=("Zebra Crossing")
						  	prompt=1
						fi
					fi

					SUB='bus'
					if [[ "$STR" == *"$SUB"* ]]; then
						SUB='squares'
						if [[ "$STR" == *"$SUB"* ]]; then
		  				  	echo "PUZZLE::bus 4x4"
		  				  	Puzzle=('"Name": "Bus"' '"Name": "Truck"')
		  				  	string=("Bus" "Truck")
		  				  	prompt=1
						else
							echo "PUZZLE::bus 3x3"
							if [ $4 -le 3 ]; then
			  				  	Puzzle=('"Name": "Bus"' '"Name": "Truck"')
			  				  	string=("Bus" "Truck")
								echo "Google always blurs these images when V3 score is low, skipping"
								prompt=-1
							else
			  				  	Puzzle=('"Name": "Bus"' '"Name": "Truck"')
			  				  	string=("Bus" "Truck")
								echo "Rekognition sucks with these... kipping"
							  	prompt=-1
							fi
  				  	
					    fi
					fi
		
					SUB='parking meter'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::parking meter"
					  Puzzle=('"Name": "Parking Meter"')
					  string=("Parking Meter")
					  prompt=1
					fi

					SUB='boat'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::boat"
					  Puzzle=('"Name": "Boat"')
					  string=("Boat")
					  prompt=1
					fi

					SUB='bicycles'
					if [[ "$STR" == *"$SUB"* ]]; then
						SUB='squares'
						if [[ "$STR" == *"$SUB"* ]]; then
						  	echo "PUZZLE::Bikes 4x4"
				  		    Puzzle=('"Name": "Bicycle"' '"Name": "Bike"' '"Name": "Cyclist"')
				  		    string=("Bicycle" "Bike" "Cyclist")
						  	prompt=1
						else
						    echo "PUZZLE::bike 3x3"
				  		    Puzzle=('"Name": "Bicycle"' '"Name": "Bike"' '"Name": "Cyclist"')
				  		    string=("Bicycle" "Bike" "Cyclist")
						  	prompt=1
					    fi
					fi

					SUB='vehicles'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::vehicle"
					  Puzzle=('"Name": "Vehicle"' '"Name": "Car"' '"Name": "Bus"')
					  string=("Vehicle" "Car" "Automobile" "Bus" "Truck")
					  prompt=1
					fi
		
					SUB='tractor'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::tractor"
					  Puzzle=('"Name": "Tractor"')
					  string=("Tractor")
					  prompt=1
					fi

					SUB='car'
					if [[ "$STR" == *"$SUB"* ]]; then
						SUB='squares'
						if [[ "$STR" == *"$SUB"* ]]; then
						  	echo "PUZZLE::car 4x4"
						  	Puzzle=('"Name": "Car"' '"Name": "Automobile"' '"Name": "Vehicle"')
						  	string=("Car" "Automobile" "Vehicle")
						  	prompt=1
						else
							SUB='left'
							if [[ "$STR" == *"$SUB"* ]]; then
								echo "PUZZLE::car 3x3 noneleft"
							  	Puzzle=('"Name": "Car"' '"Name": "Automobile"' '"Name": "Vehicle"')
							  	string=("Car" "Automobile" "Vehicle")
								echo "Google always blurs these images, skipping"
							  	prompt=-1
							else
								echo "PUZZLE::car 3x3 simple"
							  	Puzzle=('"Name": "Car"' '"Name": "Automobile"' '"Name": "Vehicle"')
							  	string=("Car" "Automobile" "Vehicle")
								echo "Google always blurs these images, skipping"
							  	prompt=-1
							fi
					    fi
					fi

					SUB='chimney'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::chimney"
					  Puzzle=('"Name": "Chimney"')
					  string=("Chimney")
					  echo "Rekognition sucks at regognizing these. Asking for another puzzle"
					  prompt=-1
					fi

					SUB='bridge'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::bridges"
					  Puzzle=('"Name": "Bridge"')
					  string=("Bridge")
					  echo "Bridges can't be recognized. Asking for another puzzle"
					  prompt=-1
					fi

					SUB='stairs'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::stairs"
					  Puzzle=('"Name": "Stair"')
					  string=("Stair")
					  echo "can't recognize stairs"
					  prompt=-1
					fi
		
					SUB='statue'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::statue"
					  Puzzle=('"Name": "Monument"' '"Name": "Sculpture"' '"Name": "Statue"')
					  string=("Monument" "Sculpture" "Statue")
					  prompt=1
					fi

					SUB='taxi'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::taxis"
					  Puzzle=('"Name": "Taxi"' '"Name": "Cab"')
					  string=("Taxi" "Cab")
					  prompt=1
					fi

					SUB='mountains'
					if [[ "$STR" == *"$SUB"* ]]; then
					  echo "PUZZLE::Mountains"
					  Puzzle=('"Name": "Mountain"')
					  string=("Mountain")
					  echo "mountains::Not supported"
					  prompt=-1
					fi
				fi
	
				echo $waitCount
				((waitCount=waitCount+1))
				if [ $waitCount -eq 200 ]; then
					echo "An error has occurred while waiting for Rekognition. Skipping"
					waitForLabels=1
					skip=1
				fi 
				sleep 0.05
			done
		fi
		
	  	# if prompt==0  is still true at this point then its unrecognized
	  	# if prompt==-1 is still true at this point then its unsupported
		if [ $prompt -eq 0 ] || [ $prompt -eq -1 ]; then
			# If V3 score is less or equal to 0.3
			if [ $4 -le 3 ]; then
				# We will actually ask for a new puzzle up to 3 times. Captcha is alerted to our presence...
				# Next best thing is to get a 3x3 we can solve. 4x4 is almost impossible now
			  	if [ $3 -le 2 ]; then
					echo "Clicking on refresh 2" $3
				  	xdotool mousemove $((300+$1)) $((1430+$2)) click 1 #235 1520
				  	sleep 2
			  	  	./main.sh $1 $2 $(($3+1)) $4
			  	else
				  	echo "No more attempts on 3x3. Skipping" $3
			  	fi
			else
			  	if [ $3 -le 1 ]; then
					echo "Clicking on refresh 2" $3
				  	xdotool mousemove $((300+$1)) $((1430+$2)) click 1 #235 1520
				  	sleep 2
			  	  	./main.sh $1 $2 $(($3+1)) $4
			  	else
				  	echo "No more attempts on 3x3. Skipping" $3
			  	fi
			fi	
		else
			waitForLabels=0
			waitCount=0
			# If we waited for text from Rekognition then as soon as we check for labels its gonna resolve to true if the labels have loaded
			# If we got prompt and puzzle type from Tesseract, then we just wait for the labels below
			while [ $waitForLabels -eq 0 ]
			do
				STR=`cat text.txt`
	
				SUB='squares'
				if [[ "$STR" == *"$SUB"* ]]; then
					if [[ $crosswalk -eq 1 ]]; then
						if [ -s 1_1.txt  ] && [ -s 1_2.txt ] && [ -s 1_3.txt ] && [ -s 1_4.txt ] && [ -s 2_1.txt ] && [ -s 2_2.txt ] && [ -s 2_3.txt ] && [ -s 2_4.txt ] && [ -s 3_1.txt ] && [ -s 3_2.txt ] && [ -s 3_3.txt ] && [ -s 3_4.txt ] && [ -s 4_1.txt ] && [ -s 4_2.txt ] && [ -s 4_3.txt ] && [ -s 4_4.txt ]; then
							echo "Labels loaded for crosswalk 4x4"
							waitForLabels=1
						fi
					else
						if [ -s image.txt  ]; then
							echo "Labels loaded for normal 4x4"
							waitForLabels=1
						fi
					fi
				fi
	
				SUB='all images'
				if [[ "$STR" == *"$SUB"* ]]; then
					if [ -s 1_1.txt  ] && [ -s 1_2.txt ] && [ -s 1_3.txt ] && [ -s 2_1.txt ] && [ -s 2_2.txt ] && [ -s 2_3.txt ] && [ -s 3_1.txt ] && [ -s 3_2.txt ] && [ -s 3_3.txt ]; then
						echo "Labels loaded 3x3"
						waitForLabels=1
					fi
				fi
	
				echo $waitCount
				((waitCount=waitCount+1))
				if [ $waitCount -eq 200 ]; then
					echo "An error has occurred while waiting for Rekognition. Skipping"
					waitForLabels=1
					puzzleType=20 #Quick and dirty to skip all the way to the end
				fi 
				sleep 0.05
			done


			#Process simple 3x3
			if [ $puzzleType -eq 1 ];  then
				V1_1=0
				V1_2=0
				V1_3=0
				V2_1=0
				V2_2=0
				V2_3=0
				V3_1=0
				V3_2=0
				V3_3=0

				noMatch=0
			  for i in "${Puzzle[@]}"
				do 
			 	  value=`cat 1_1.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "1_1"
					V1_1=1 
					noMatch=1
			 	  fi

			 	  value=`cat 1_2.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "1_2"
					V1_2=1
					noMatch=1
			 	  fi

			 	  value=`cat 1_3.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "1_3"
					V1_3=1
					noMatch=1
			 	  fi

			 	  value=`cat 2_1.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "2_1"
					V2_1=1
					noMatch=1
			 	  fi

			 	  value=`cat 2_2.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "2_2"
					V2_2=1
					noMatch=1
			 	  fi

			 	  value=`cat 2_3.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "2_3"
					V2_3=1
					noMatch=1
			 	  fi

			 	  value=`cat 3_1.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "3_1"
					V3_1=1
					noMatch=1
			 	  fi

			 	  value=`cat 3_2.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "3_2"
					V3_2=1
					noMatch=1
			 	  fi

			 	  value=`cat 3_3.txt`
			 	  if [[ "$value" == *"$i"* ]]; then
			 	  	echo "3_3"
					V3_3=1
					noMatch=1
			 	  fi
				done

				#click on results
				if [ $noMatch -eq 0 ]; then
					echo "No results to click 3x3"
					xdotool mousemove $((965+$1)) $((1490+$2)) click 1 #900 1550 
					sleep 1.5
					
					verifyPass
					retval=$?
					if [ $retval -eq 1 ]; then
						Pass
						echo "PASS"
					else
						echo "Failed to pass. Switching DNS"
						Fail
					fi
				else
					if [ $V1_1 -eq 1 ]; then
						echo "1_1 press"
						xdotool mousemove $((400+$1)) $((600+$2)) click 1 #1_1
					fi

					if [ $V1_2 -eq 1 ]; then
						echo "1_2 press"
						xdotool mousemove $((600+$1)) $((700+$2)) click 1 #1_2
					fi

					if [ $V1_3 -eq 1 ]; then
						echo "1_3 press"
						xdotool mousemove $((900+$1)) $((650+$2)) click 1 #1_3
					fi

					if [ $V2_1 -eq 1 ]; then
						echo "2_1 press"
						xdotool mousemove $((400+$1)) $((900+$2)) click 1 #2_1
					fi

					if [ $V2_2 -eq 1 ]; then
						echo "2_2 press"
						xdotool mousemove $((600+$1)) $((900+$2)) click 1 #2_2
					fi

					if [ $V2_3 -eq 1 ]; then
						echo "2_3 press"
						xdotool mousemove $((900+$1)) $((900+$2)) click 1 #2_3
					fi

					if [ $V3_1 -eq 1 ]; then
						echo "3_1 press"
						xdotool mousemove $((400+$1)) $((1200+$2)) click 1 #3_1
					fi

					if [ $V3_2 -eq 1 ]; then
						echo "3_2 press"
						xdotool mousemove $((600+$1)) $((1200+$2)) click 1 #3_2
					fi

					if [ $V3_3 -eq 1 ]; then
						echo "3_3 press"
						xdotool mousemove $((900+$1)) $((1200+$2)) click 1 #3_3
					fi
				fi

				xdotool mousemove $((1200+$1)) $((400+$2))

				#click on verify
				xdotool mousemove $((965+$1)) $((1490+$2)) click 1 #900 1550
				sleep 1.5
				
				verifyPass
				retval=$?
				if [ $retval -eq 1 ]; then
					Pass
					echo "PASS"
				else
					echo "Failed to pass. Switching DNS"
					Fail
				fi
			fi

			#Process 4x4
			if [ $puzzleType -eq 2 ]; then
				SUB='crosswalk' 
				if [[ "$STR" == *"$SUB"* ]]; then
					echo "PROCESSING ZEBRA CROSSING 4x4"
				  	V1_1=0
				  	V1_2=0
				  	V1_3=0
					V1_4=0
				  	V2_1=0
				  	V2_2=0
				  	V2_3=0
					V2_4=0
				  	V3_1=0
				  	V3_2=0
				  	V3_3=0
					V3_4=0
				  	V4_1=0
				  	V4_2=0
				  	V4_3=0
					V4_4=0
					noMatch=0

					  zeb='"Name": "Zebra Crossing"'
				   	  value=`cat 1_1.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "1_1"
				  		V1_1=1 
						noMatch=1
				   	  fi

				   	  value=`cat 1_2.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "1_2"
				  		V1_2=1
						noMatch=1
				   	  fi

				   	  value=`cat 1_3.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "1_3"
				  		V1_3=1
						noMatch=1
				   	  fi

				   	  value=`cat 1_4.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "1_4"
				  		V1_4=1
						noMatch=1
				   	  fi

				   	  value=`cat 2_1.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "2_1"
				  		V2_1=1
						noMatch=1
				   	  fi

				   	  value=`cat 2_2.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "2_2"
				  		V2_2=1
						noMatch=1
				   	  fi

				   	  value=`cat 2_3.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "2_3"
				  		V2_3=1
						noMatch=1
				   	  fi

				   	  value=`cat 2_4.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "2_4"
				  		V2_4=1
						noMatch=1
				   	  fi

				   	  value=`cat 3_1.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "3_1"
				  		V3_1=1
						noMatch=1
				   	  fi

				   	  value=`cat 3_2.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "3_2"
				  		V3_2=1
						noMatch=1
				   	  fi

				   	  value=`cat 3_3.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "3_3"
				  		V3_3=1
						noMatch=1
				   	  fi

				   	  value=`cat 3_4.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "3_4"
				  		V3_4=1
						noMatch=1
				   	  fi

				   	  value=`cat 4_1.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "4_1"
				  		V4_1=1
						noMatch=1
				   	  fi

				   	  value=`cat 4_2.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "4_2"
				  		V4_2=1
						noMatch=1
				   	  fi

				   	  value=`cat 4_3.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "4_3"
				  		V4_3=1
						noMatch=1
				   	  fi

				   	  value=`cat 4_4.txt`
				   	  if [[ "$value" == *"$zeb"* ]]; then
				   	  	echo "4_4"
				  		V4_4=1
						noMatch=1
				   	  fi

				  	#click on results
				  	if [ $noMatch -eq 0 ]; then
				  		echo "4x4 zero matches. Clicking Blue button"
						xdotool mousemove $((965+$1)) $((1490+$2)) click 1 #900 1550
						passOrFail=0
						sleep 1.5
						
						maim -g 360x90+1360+2070 error.png error.png # reddit
						
						STR=$(tesseract error.png stdout)	
						SUB='try again'
						SUB2='select all'
                        
						if [[ "$STR" == *"$SUB"* ]] || [[ "$STR" == *"$SUB2"* ]]; then
						  echo "Failed to pass. Switching DNS"
						  passOrFail=1
						  Fail
						fi
			
						verifyPass
						retval=$?
						if [ $retval -eq 1 ]; then
							echo "PASS"
							passOrFail=1
							Pass
						fi
			
						if [ $passOrFail -eq 0 ]; then
							echo "We got another puzzle"
							./main.sh $1 $2 $(($3+1)) $4
						fi
				  	else
				  		if [ $V1_1 -eq 1 ]; then
				  			echo "1_1 press"
							xdotool mousemove $((300+$1)) $((600+$2)) click 1 #1_1	
				  		fi

				  		if [ $V1_2 -eq 1 ]; then
				  			echo "1_2 press"
				  			xdotool mousemove $((500+$1)) $((600+$2)) click 1 #1_2
				  		fi

				  		if [ $V1_3 -eq 1 ]; then
				  			echo "1_3 press"
				  			xdotool mousemove $((690+$1)) $((600+$2)) click 1 #1_3
				  		fi

				  		if [ $V1_4 -eq 1 ]; then
				  			echo "1_4 press"
				  			xdotool mousemove $((900+$1)) $((600+$2)) click 1 #1_4
				  		fi

				  		if [ $V2_1 -eq 1 ]; then
				  			echo "2_1 press"
				  			xdotool mousemove $((300+$1)) $((800+$2)) click 1 #2_1
				  		fi

				  		if [ $V2_2 -eq 1 ]; then
				  			echo "2_2 press"
				  			xdotool mousemove $((500+$1)) $((800+$2)) click 1 #2_2
				  		fi

				  		if [ $V2_3 -eq 1 ]; then
				  			echo "2_3 press"
				  			xdotool mousemove $((690+$1)) $((800+$2)) click 1 #2_3
				  		fi

				  		if [ $V2_4 -eq 1 ]; then
				  			echo "2_4 press"
				  			xdotool mousemove $((900+$1)) $((800+$2)) click 1 #2_4
				  		fi

				  		if [ $V3_1 -eq 1 ]; then
				  			echo "3_1 press"
				  			xdotool mousemove $((300+$1)) $((1000+$2)) click 1 #3_1
				  		fi

				  		if [ $V3_2 -eq 1 ]; then
				  			echo "3_2 press"
				  			xdotool mousemove $((500+$1)) $((1000+$2)) click 1 #3_2
				  		fi

				  		if [ $V3_3 -eq 1 ]; then
				  			echo "3_3 press"
				  			xdotool mousemove $((690+$1)) $((1000+$2)) click 1 #3_3
				  		fi

				  		if [ $V3_4 -eq 1 ]; then
				  			echo "3_4 press"
				  			xdotool mousemove $((900+$1)) $((1000+$2)) click 1 #3_4
				  		fi

				  		if [ $V4_1 -eq 1 ]; then
				  			echo "4_1 press"
				  			xdotool mousemove $((300+$1)) $((1200+$2)) click 1 #4_1
				  		fi

				  		if [ $V4_2 -eq 1 ]; then
				  			echo "4_2 press"
				  			xdotool mousemove $((500+$1)) $((1200+$2)) click 1 #4_2
				  		fi

				  		if [ $V4_3 -eq 1 ]; then
				  			echo "4_3 press"
				  			xdotool mousemove $((690+$1)) $((1200+$2)) click 1 #4_3
				  		fi

				  		if [ $V4_4 -eq 1 ]; then
				  			echo "4_4 press"
				  			xdotool mousemove $((900+$1)) $((1200+$2)) click 1 #4_4
				  		fi
	
					  	#click on verify
				  		echo "4x4 Clicking Blue button ==1=="
						xdotool mousemove $((965+$1)) $((1490+$2)) click 1 #900 1550
						passOrFail=0
						sleep 1.5
						
						maim -g 360x90+1360+2070 error.png 
						
						
						#1230 2115
						
						STR=$(tesseract error.png stdout)	
						SUB='try again'
						SUB2='select all'
                        
						if [[ "$STR" == *"$SUB"* ]] || [[ "$STR" == *"$SUB2"* ]]; then
						  echo "Failed to pass. Switching DNS"
						  passOrFail=1
						  Fail
						fi
			
						verifyPass
						retval=$?
						if [ $retval -eq 1 ]; then
							echo "PASS"
							passOrFail=1
							Pass
						fi
			
						if [ $passOrFail -eq 0 ]; then
							echo "We got another puzzle"
							./main.sh $1 $2 $(($3+1)) $4
						fi
				  	fi 	
				else
					declare arr
					arr[0]=0
					arr[1]=0
					arr[2]=0
					arr[3]=0
					arr[4]=0
					arr[5]=0
					arr[6]=0
					arr[7]=0
					arr[8]=0
					arr[9]=0
					arr[10]=0
					arr[11]=0
					arr[12]=0
					arr[13]=0
					arr[14]=0
					arr[15]=0

					for i in "${string[@]}"
					do
						echo "string:" $i

						#echo ".Labels[] | select(.Name==\"$i\") | .Instances"
						#cat image.txt | jq ".Labels[] | select(.Name==\"$i\") | .Instances"

						imageWidth=865
						imageHeight=865
						sideLen=212

						response=`cat image.txt`
						echo $response

						for row in $(cat image.txt | jq -c ".Labels[] | select(.Name==(\"$i\")) | .Instances | .[]"); do
						#for row in $(cat image.txt | jq -c '.Labels[] | select(.Name==("Car")) | .Instances | .[]'); do
						    counter=0
						    for line in $( echo $row | jq -r '.BoundingBox | .[]'); do
								echo $line
						         if [ $counter -eq 0 ]; then
						             width=$(echo "$line * $imageWidth" | bc )
									 w=$(echo "$width * 0.8" | bc )
								     w=${w%.*}
									 #echo "w" $w
						         fi

						         if [ $counter -eq 1 ]; then
						             height=$(echo "$line * $imageHeight" | bc )
									 h=$(echo "$height * 0.8" | bc )
								     h=${h%.*}
									 #echo "h" $h
						         fi

						         if [ $counter -eq 2 ]; then
									 dx=$(echo "$width * 0.1" | bc )
						             x=$(echo "$line * $imageWidth" | bc )
									 x=$(echo "$x + $dx" | bc )
								     x=${x%.*}
									 #echo "x" $x
						         fi

						         if [ $counter -eq 3 ]; then
									 dy=$(echo "$height * 0.1" | bc )
						             y=$(echo "$line * $imageHeight" | bc )
									 y=$(echo "$y + $dy" | bc )
								     y=${y%.*}
									 #echo "y" $y
						         fi
						         ((counter=counter+1))
						    done

							a=$((($x/$sideLen)+(($y/$sideLen)*4)))
							#b=$(((($x+$w)/$sideLen)+(($y/$sideLen)*4)))
							#c=$((($x/$sideLen)+((($y+$h)/$sideLen)*4)))
							#d=$(((($x+$w)/$sideLen)+((($y+$h)/$sideLen)*4)))

							#echo $a $b $c $d
							#echo $(((($y+$h)/$sideLen)-($y/$sideLen)))	
							#echo $(((($x+$w)/$sideLen)-($x/$sideLen)))

							for (( yy = 0; yy <= (((($y+$h)/$sideLen)-($y/$sideLen))); yy++ ))  
							do
							    for (( xx = 0 ; xx <= (((($x+$w)/$sideLen)-($x/$sideLen))); xx++ ))
							    do
									p=$(($a+$xx+($yy*4)))
									#echo $p
									#$((($x+$xx)/$sideLen+((($y+$yy)/$sideLen)*4)))
									arr[$p]=1
							    done
							done
						done
					done

					echo ""
					echo ""
					for (( y = 0; y < 4; y++ )) 
					do
					   for (( x = 0 ; x < 4; x++ ))
					   do
					        ind=$(( $x + $(($y * 4))))   
					        if [ ${arr[$ind]} -eq 0 ]; then
								echo -e -n "██"
							else
					            echo -e -n "╳╳"
							fi
					  done
					  echo ""
					done
					echo ""
					echo ""

					echo ${arr[*]}

					if [ ${arr[0]}  -eq 1 ]; then
						echo "1_1 press"
						xdotool mousemove $((300+$1)) $((600+$2)) click 1 #1_1	
					fi

					if [ ${arr[1]}  -eq 1 ]; then
						echo "1_2 press"
						xdotool mousemove $((500+$1)) $((600+$2)) click 1 #1_2
					fi

					if [ ${arr[2]}  -eq 1 ]; then
						echo "1_3 press"
						xdotool mousemove $((690+$1)) $((600+$2)) click 1 #1_3
					fi

					if [ ${arr[3]}  -eq 1 ]; then
						echo "1_4 press"
						xdotool mousemove $((900+$1)) $((600+$2)) click 1 #1_4
					fi

					if [ ${arr[4]}  -eq 1 ]; then
						echo "2_1 press"
						xdotool mousemove $((300+$1)) $((800+$2)) click 1 #2_1
					fi

					if [ ${arr[5]}  -eq 1 ]; then
						echo "2_2 press"
						xdotool mousemove $((500+$1)) $((800+$2)) click 1 #2_2
					fi

					if [ ${arr[6]}  -eq 1 ]; then
						echo "2_3 press"
						xdotool mousemove $((690+$1)) $((800+$2)) click 1 #2_3
					fi

					if [ ${arr[7]}  -eq 1 ]; then
						echo "2_4 press"
						xdotool mousemove $((900+$1)) $((800+$2)) click 1 #2_4
					fi

					if [ ${arr[8]}  -eq 1 ]; then
						echo "3_1 press"
						xdotool mousemove $((300+$1)) $((1000+$2)) click 1 #3_1
					fi

					if [ ${arr[9]}  -eq 1 ]; then
						echo "3_2 press"
						xdotool mousemove $((500+$1)) $((1000+$2)) click 1 #3_2
					fi

					if [ ${arr[10]}  -eq 1 ]; then
						echo "3_3 press"
						xdotool mousemove $((690+$1)) $((1000+$2)) click 1 #3_3
					fi

					if [ ${arr[11]}  -eq 1 ]; then
						echo "3_4 press"
						xdotool mousemove $((900+$1)) $((1000+$2)) click 1 #3_4
					fi

					if [ ${arr[12]}  -eq 1 ]; then
						echo "4_1 press"
						xdotool mousemove $((300+$1)) $((1200+$2)) click 1 #4_1
					fi

					if [ ${arr[13]}  -eq 1 ]; then
						echo "4_2 press"
						xdotool mousemove $((500+$1)) $((1200+$2)) click 1 #4_2
					fi

					if [ ${arr[14]}  -eq 1 ]; then
						echo "4_3 press"
						xdotool mousemove $((690+$1)) $((1200+$2)) click 1 #4_3
					fi

					if [ ${arr[15]}  -eq 1 ]; then
						echo "4_4 press"
						xdotool mousemove $((900+$1)) $((1200+$2)) click 1 #4_4
					fi
	
					#click on verify
			  		echo "4x4 Clicking Blue button"
					xdotool mousemove $((965+$1)) $((1490+$2)) click 1 #900 1550
					passOrFail=0
					sleep 1.5
					
					#maim -g 360x90+$((487+$1))+$((1394+$2)) error.png # reddit
					maim -g 360x90+1360+2070 error.png 
					
					STR=$(tesseract error.png stdout)	
					SUB='try again'
					SUB2='select all'
                    
					if [[ "$STR" == *"$SUB"* ]] || [[ "$STR" == *"$SUB2"* ]]; then
					  echo "Failed to pass. Switching DNS"
					  passOrFail=1
					  Fail
					fi
		
					verifyPass
					retval=$?
					if [ $retval -eq 1 ]; then
						echo "PASS"
						passOrFail=1
						Pass
					fi
		
					if [ $passOrFail -eq 0 ]; then
						echo "We got another puzzle " $(($3+1))
						./main.sh $1 $2 $(($3+1)) $4
					fi	  		
				fi
			fi

			#Process 3x3 none left
			if [ $puzzleType -eq 3 ];  then
				noMatch=1
				while [ $noMatch -eq 1 ]     
				do
					V1_1=0
					V1_2=0
					V1_3=0
					V2_1=0
					V2_2=0
					V2_3=0
					V3_1=0
					V3_2=0
					V3_3=0

					noMatch=0

					echo ${Puzzle[*]}
					for i in "${Puzzle[@]}"
					do 
					echo $i
					  value=`cat 1_1.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "1_1"
						V1_1=1 
						noMatch=1
					  fi

					  value=`cat 1_2.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "1_2"
						V1_2=1
						noMatch=1
					  fi

					  value=`cat 1_3.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "1_3"
						V1_3=1
						noMatch=1
					  fi

					  value=`cat 2_1.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "2_1"
						V2_1=1
						noMatch=1
					  fi

					  value=`cat 2_2.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "2_2"
						V2_2=1
						noMatch=1
					  fi

					  value=`cat 2_3.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "2_3"
						V2_3=1
						noMatch=1
					  fi

					  value=`cat 3_1.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "3_1"
						V3_1=1
						noMatch=1
					  fi

					  value=`cat 3_2.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "3_2"
						V3_2=1
						noMatch=1
					  fi

					  value=`cat 3_3.txt`
					  if [[ "$value" == *"$i"* ]]; then
					  	#echo "3_3"
						V3_3=1
						noMatch=1
					  fi
					done

					#click on results
					if [ $noMatch -eq 0 ]; then
						echo "No results to click 3x3 none-left"
						xdotool mousemove $((965+$1)) $((1490+$2)) click 1 #900 1550
						sleep 1.5
						verifyPass
						retval=$?
						if [ $retval -eq 1 ]; then
							Pass
							echo "PASS"
						else
							echo "Failed to pass. Switching DNS"
							Fail
							noMatch=0 # Quick and dirty to exit loop
						fi
					else
						if [ "$V1_1" -eq 1 ]; then
							echo "1_1 press"
							xdotool mousemove $((400+$1)) $((600+$2)) click 1 #1_1
						fi

						if [ "$V1_2" -eq 1 ]; then
							echo "1_2 press"
							xdotool mousemove $((600+$1)) $((700+$2)) click 1 #1_2
						fi

						if [ "$V1_3" -eq 1 ]; then
							echo "1_3 press"
							xdotool mousemove $((900+$1)) $((650+$2)) click 1 #1_3
						fi

						if [ "$V2_1" -eq 1 ]; then
							echo "2_1 press"
							xdotool mousemove $((400+$1)) $((900+$2)) click 1 #2_1
						fi

						if [ "$V2_2" -eq 1 ]; then
							echo "2_2 press"
							xdotool mousemove $((600+$1)) $((900+$2)) click 1 #2_2
						fi

						if [ "$V2_3" -eq 1 ]; then
							echo "2_3 press"
							xdotool mousemove $((900+$1)) $((900+$2)) click 1 #2_3
						fi

						if [ "$V3_1" -eq 1 ]; then
							echo "3_1 press"
							xdotool mousemove $((400+$1)) $((1200+$2)) click 1 #3_1
						fi

						if [ "$V3_2" -eq 1 ]; then
							echo "3_2 press"
							xdotool mousemove $((600+$1)) $((1200+$2)) click 1 #3_2
						fi

						if [ "$V3_3" -eq 1 ]; then
							echo "3_3 press"
							xdotool mousemove $((900+$1)) $((1200+$2)) click 1 #3_3
						fi
	
						xdotool mousemove 1 1

						echo "Waiting for new images"
						imageWait=0
						while [ $imageWait -le 20 ]  
						do
					
							maim -g 865x865+$((250+$1))+$((525+$2)) old_snap.png &
							if [ $4 -le 3 ]; then
								sleep 4
							else
								sleep 2.5 # Time delta, or dt
							fi
							maim -g 865x865+$((250+$1))+$((525+$2)) new_snap.png &
							diff old_snap.png new_snap.png > snap_dt.txt
							if [ ! -s snap_dt.txt ]; then
								echo "Images finished loading"
								imageWait=21 # Quick and dirty to leave loop
							fi
						done
	
						#retake snapshots
						if [ "$V1_1" -eq 1 ]; then
							echo "1_1 snap"
							maim -g 290x290+$((250+$1))+$((525+$2)) notabot/q_1_1.png &
						fi

						if [ "$V1_2" -eq 1 ]; then
							echo "1_2 snap"
							maim -g 290x290+$((530+$1))+$((525+$2)) notabot/q_1_2.png &
						fi

						if [ "$V1_3" -eq 1 ]; then
							echo "1_3 snap"
							maim -g 290x290+$((830+$1))+$((525+$2)) notabot/q_1_3.png &
						fi

						if [ "$V2_1" -eq 1 ]; then
							echo "2_1 snap"
							maim -g 290x290+$((250+$1))+$((815+$2)) notabot/q_2_1.png &
						fi

						if [ "$V2_2" -eq 1 ]; then
							echo "2_2 snap"
							maim -g 290x290+$((530+$1))+$((815+$2)) notabot/q_2_2.png &
						fi

						if [ "$V2_3" -eq 1 ]; then
							echo "2_3 snap"
							maim -g 290x290+$((830+$1))+$((815+$2)) notabot/q_2_3.png &
						fi

						if [ "$V3_1" -eq 1 ]; then
							echo "3_1 snap"
							maim -g 290x290+$((250+$1))+$((1100+$2)) notabot/q_3_1.png &
						fi

						if [ "$V3_2" -eq 1 ]; then
							echo "3_2 snap"
							maim -g 290x290+$((530+$1))+$((1100+$2)) notabot/q_3_2.png &
						fi

						if [ "$V3_3" -eq 1 ]; then
							echo "3_3 snap"
							maim -g 290x290+$((830+$1))+$((1100+$2)) notabot/q_3_3.png &
						fi

						sleep 0.1

						#upload new screenshots to S3
						cd notabot
						aws s3 sync . s3://voightkampff --exclude "prompt.png" --exclude "error.png"
						cd ..

						if [ "$V1_1" -eq 1 ]; then
							echo "1_1 process"
							rm 1_1.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_1_1.png"}}' > 1_1.txt &
						fi

						if [ "$V1_2" -eq 1 ]; then
							echo "1_2 process"
							rm 1_2.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_1_2.png"}}' > 1_2.txt &
						fi

						if [ "$V1_3" -eq 1 ]; then
							echo "1_3 process"
							rm 1_3.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_1_3.png"}}' > 1_3.txt &
						fi

						if [ "$V2_1" -eq 1 ]; then
							echo "2_1 process"
							rm 2_1.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_2_1.png"}}' > 2_1.txt &
						fi

						if [ "$V2_2" -eq 1 ]; then
							echo "2_2 process"
							rm 2_2.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_2_2.png"}}' > 2_2.txt &
						fi

						if [ "$V2_3" -eq 1 ]; then
							echo "2_3 process"
							rm 2_3.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_2_3.png"}}' > 2_3.txt &
						fi

						if [ "$V3_1" -eq 1 ]; then
							echo "3_1 process"
							rm 3_1.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_3_1.png"}}' > 3_1.txt &
						fi

						if [ "$V3_2" -eq 1 ]; then
							echo "3_2 process"
							rm 3_2.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_3_2.png"}}' > 3_2.txt &
						fi

						if [ "$V3_3" -eq 1 ]; then
							echo "3_3 process"
							rm 3_3.txt
							aws rekognition detect-labels --image '{"S3Object":{"Bucket":"voightkampff","Name":"q_3_3.png"}}' > 3_3.txt &
						fi

						echo "Waiting for labels"
						waitForLabels=0
						while [ $waitForLabels -eq 0 ]
						do
							if [ -s 1_1.txt  ] && [ -s 1_2.txt ] && [ -s 1_3.txt ] && [ -s 2_1.txt ] && [ -s 2_2.txt ] && [ -s 2_3.txt ] && [ -s 3_1.txt ] && [ -s 3_2.txt ] && [ -s 3_3.txt ]; then
								waitForLabels=1
							fi
						done
						echo "Labels loaded"
					fi
				done
			fi
		fi
	else
		verifyPass
		retval=$?
		if [ $retval -eq 1 ]; then
			Pass
			echo "PASS"
		else
			echo "Could not identify puzzle type"
			# If V3 score is less or equal to 0.3
			if [ $4 -le 3 ]; then
			  	if [ $3 -le 2 ]; then
					echo "Clicking on refresh 2" $3
				  	xdotool mousemove $((300+$1)) $((1430+$2)) click 1 #235 1520
				  	sleep 2
			  	  	./main.sh $1 $2 $(($3+1)) $4
			  	else
				  	echo "No more attempts on 3x3. Skipping" $3
			  	fi
			else
			  	if [ $3 -le 1 ]; then
					echo "Clicking on refresh 2" $3
				  	xdotool mousemove $((300+$1)) $((1430+$2)) click 1 #235 1520
				  	sleep 2
			  	  	./main.sh $1 $2 $(($3+1)) $4
			  	else
				  	echo "No more attempts on 3x3. Skipping" $3
			  	fi
			fi
		fi
	fi
fi




