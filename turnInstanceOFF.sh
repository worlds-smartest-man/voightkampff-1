#!/bin/bash
declare -a stopInstances
stopInstances[0]="aws ec2 stop-instances --instance-ids i-09be868e7251ff275   --region us-east-1"
stopInstances[1]="aws ec2 stop-instances --instance-ids i-018a71a8b62711ac2   --region us-east-2"
stopInstances[2]="aws ec2 stop-instances --instance-ids i-034c955aab1114687   --region us-west-1"
stopInstances[3]="aws ec2 stop-instances --instance-ids i-043855242f72de89d   --region us-west-2"
stopInstances[4]="aws ec2 stop-instances --instance-ids i-01acfd1bd855c7883   --region ap-northeast-2"
stopInstances[5]="aws ec2 stop-instances --instance-ids i-02b0d141e948a1136   --region ap-southeast-1"
stopInstances[6]="aws ec2 stop-instances --instance-ids i-08310b97b301f6cd0   --region ap-southeast-2"
stopInstances[7]="aws ec2 stop-instances --instance-ids i-06c938bc5e8370d40   --region eu-central-1"
stopInstances[8]="aws ec2 stop-instances --instance-ids i-00c9d6ac2ff058c97   --region af-south-1"  
stopInstances[9]="aws ec2 stop-instances --instance-ids i-01dbb61c54d755309  --region ap-east-1"  
stopInstances[10]="aws ec2 stop-instances --instance-ids i-03f25a7e427fe6269  --region ap-south-1"  
stopInstances[11]="aws ec2 stop-instances --instance-ids i-0583d39963763bd91  --region ap-northeast-1"  
stopInstances[12]="aws ec2 stop-instances --instance-ids i-05bceff97897797f8  --region ca-central-1"  
stopInstances[13]="aws ec2 stop-instances --instance-ids i-04bc5c446108c364b  --region eu-west-1"  
stopInstances[14]="aws ec2 stop-instances --instance-ids i-02c8ccada959c93b0  --region eu-west-2"  
stopInstances[15]="aws ec2 stop-instances --instance-ids i-0739d160bcd3366ca  --region eu-south-1"  
stopInstances[16]="aws ec2 stop-instances --instance-ids i-034dac54659ed8d3c  --region eu-west-3"  
stopInstances[17]="aws ec2 stop-instances --instance-ids i-00f05adc1319e669f  --region eu-north-1"  
stopInstances[18]="aws ec2 stop-instances --instance-ids i-00a50e0e47faf9545  --region me-south-1"  
stopInstances[19]="aws ec2 stop-instances --instance-ids i-0429661f7f9fe75fd  --region sa-east-1"  

${stopInstances[$1]}
