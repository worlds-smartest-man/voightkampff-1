#!/bin/bash
echo ">>switchDNS.sh"

./closeDNS.sh
./turnInstanceOFF.sh $1
./turnInstanceON.sh $((($1 + 1) % 20))

declare -a getDNS
getDNS[0]="aws ec2 describe-instances --instance-ids i-09be868e7251ff275   --region us-east-1        --query" 
getDNS[1]="aws ec2 describe-instances --instance-ids i-018a71a8b62711ac2   --region us-east-2        --query" 
getDNS[2]="aws ec2 describe-instances --instance-ids i-034c955aab1114687   --region us-west-1        --query"  
getDNS[3]="aws ec2 describe-instances --instance-ids i-043855242f72de89d   --region us-west-2        --query" 
getDNS[4]="aws ec2 describe-instances --instance-ids i-01acfd1bd855c7883   --region ap-northeast-2   --query" 
getDNS[5]="aws ec2 describe-instances --instance-ids i-02b0d141e948a1136   --region ap-southeast-1   --query" 
getDNS[6]="aws ec2 describe-instances --instance-ids i-08310b97b301f6cd0   --region ap-southeast-2   --query" 
getDNS[7]="aws ec2 describe-instances --instance-ids i-06c938bc5e8370d40   --region eu-central-1     --query" 
getDNS[8]="aws ec2 describe-instances --instance-ids i-00c9d6ac2ff058c97   --region af-south-1       --query" 
getDNS[9]="aws ec2 describe-instances --instance-ids i-01dbb61c54d755309   --region ap-east-1        --query" 
getDNS[10]="aws ec2 describe-instances --instance-ids i-03f25a7e427fe6269  --region ap-south-1       --query" 
getDNS[11]="aws ec2 describe-instances --instance-ids i-0583d39963763bd91  --region ap-northeast-1   --query" 
getDNS[12]="aws ec2 describe-instances --instance-ids i-05bceff97897797f8  --region ca-central-1     --query" 
getDNS[13]="aws ec2 describe-instances --instance-ids i-04bc5c446108c364b  --region eu-west-1        --query" 
getDNS[14]="aws ec2 describe-instances --instance-ids i-02c8ccada959c93b0  --region eu-west-2        --query" 
getDNS[15]="aws ec2 describe-instances --instance-ids i-0739d160bcd3366ca  --region eu-south-1       --query" 
getDNS[16]="aws ec2 describe-instances --instance-ids i-034dac54659ed8d3c  --region eu-west-3        --query" 
getDNS[17]="aws ec2 describe-instances --instance-ids i-00f05adc1319e669f  --region eu-north-1       --query" 
getDNS[18]="aws ec2 describe-instances --instance-ids i-00a50e0e47faf9545  --region me-south-1       --query" 
getDNS[19]="aws ec2 describe-instances --instance-ids i-0429661f7f9fe75fd  --region sa-east-1        --query" 



declare -a tunnelCall
tunnelCall[0]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_virginia.pem    -L 3333:localhost:8888"  
tunnelCall[1]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_ohio.pem        -L 3333:localhost:8888"  
tunnelCall[2]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_california.pem  -L 3333:localhost:8888"  
tunnelCall[3]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_oregon.pem      -L 3333:localhost:8888"  
tunnelCall[4]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_seoul.pem       -L 3333:localhost:8888"  
tunnelCall[5]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_singapore.pem   -L 3333:localhost:8888"  
tunnelCall[6]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_sydney.pem      -L 3333:localhost:8888"  
tunnelCall[7]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_frankfurt.pem   -L 3333:localhost:8888"  
tunnelCall[8]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_capetown.pem    -L 3333:localhost:8888"  
tunnelCall[9]="ssh  -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_hongkong.pem    -L 3333:localhost:8888"  
tunnelCall[10]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_mumbai.pem      -L 3333:localhost:8888"  
tunnelCall[11]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_tokyo.pem       -L 3333:localhost:8888"  
tunnelCall[12]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_canada.pem      -L 3333:localhost:8888"  
tunnelCall[13]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_ireland.pem     -L 3333:localhost:8888"  
tunnelCall[14]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_london.pem      -L 3333:localhost:8888"  
tunnelCall[15]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_milan.pem       -L 3333:localhost:8888"  
tunnelCall[16]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_paris.pem       -L 3333:localhost:8888"  
tunnelCall[17]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_stockholm.pem   -L 3333:localhost:8888"  
tunnelCall[18]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_bahrain.pem     -L 3333:localhost:8888"  
tunnelCall[19]="ssh -tt -o 'StrictHostKeyChecking no' -i Downloads/aws_saopaulo.pem    -L 3333:localhost:8888"  




command=" ${getDNS[$((($1 + 1) % 20))]} 'Reservations[0].Instances[0].PublicDnsName' > dns.txt"
eval $command

value=`cat dns.txt`
temp="${value%\"}"
temp="${temp#\"}"
echo "$temp"

rm dnsStatus.txt
command=" ${tunnelCall[$((($1 + 1) % 20))]} ubuntu@$temp > dnsStatus.txt"
eval $command &
sleep 5

echo "Checking if tunneled successfully"
STR=`cat dnsStatus.txt`
SUB='Welcome to Ubuntu'
while [[ ! "$STR" == *"$SUB"* ]];
do
	eval $command &
	sleep 2
	STR=`cat dnsStatus.txt`
done
rm dnsStatus.txt



