string=("Car" "Automobile")
for i in "${string[@]}"
do	
	width=780
	height=780
	sideLen=195

	declare arr
	arr[0]=0
	arr[1]=0
	arr[2]=0
	arr[3]=0
	arr[4]=0
	arr[5]=0
	arr[6]=0
	arr[7]=0
	arr[8]=0
	arr[9]=0
	arr[10]=0
	arr[11]=0
	arr[12]=0
	arr[13]=0
	arr[14]=0
	arr[15]=0


	#for row in $(cat image.txt | jq -c ".Labels[] | select(.Name==(\"$i\")) | .Instances | .[]"); do
	for row in $(cat image.txt | jq -c '.Labels[] | select(.Name==("Car")) | .Instances | .[]'); do
	    counter=0
	    for line in $( echo $row | jq -r '.BoundingBox | .[]'); do
	         if [ $counter -eq 0 ]; then
	             w=$(echo "$line * $width" | bc )
			     w=${w%.*}
	         fi

	         if [ $counter -eq 1 ]; then
	             h=$(echo "$line * $height" | bc )
			     h=${h%.*}
	         fi

	         if [ $counter -eq 2 ]; then
	             x=$(echo "$line * $width" | bc )
			     x=${x%.*}
	         fi

	         if [ $counter -eq 3 ]; then
	             y=$(echo "$line * $height" | bc )
			     y=${y%.*}
	         fi
	         ((counter=counter+1))
	    done

		a=$((($x/$sideLen)+(($y/$sideLen)*4)))
		#b=$(((($x+$w)/$sideLen)+(($y/$sideLen)*4)))
		#c=$((($x/$sideLen)+((($y+$h)/$sideLen)*4)))
		#d=$(((($x+$w)/$sideLen)+((($y+$h)/$sideLen)*4)))

		#echo $a $b $c $d
		echo $(((($y+$h)/$sideLen)-($y/$sideLen)))	
		echo $(((($x+$w)/$sideLen)-($x/$sideLen)))
		echo =========================

		for (( yy = 0; yy <= (((($y+$h)/$sideLen)-($y/$sideLen))); yy++ ))  
		do
		    for (( xx = 0 ; xx <= (((($x+$w)/$sideLen)-($x/$sideLen))); xx++ ))
		    do
				p=$(($a+$xx+($yy*4)))
				#$((($x+$xx)/$sideLen+((($y+$yy)/$sideLen)*4)))
				echo ">" $p
				arr[$p]=1
		    done
		done
	done
done

echo "Pressing" 

if [ ${arr[0]}  -eq 1 ]; then
	echo "1_1 press"
	xdotool mousemove $((300+$1)) $((600+$2)) click 1 #1_1	
fi

if [ ${arr[1]}  -eq 1 ]; then
	echo "1_2 press"
	xdotool mousemove $((500+$1)) $((600+$2)) click 1 #1_2
fi

if [ ${arr[2]}  -eq 1 ]; then
	echo "1_3 press"
	xdotool mousemove $((690+$1)) $((600+$2)) click 1 #1_3
fi

if [ ${arr[3]}  -eq 1 ]; then
	echo "1_4 press"
	xdotool mousemove $((900+$1)) $((600+$2)) click 1 #1_4
fi

if [ ${arr[4]}  -eq 1 ]; then
	echo "2_1 press"
	xdotool mousemove $((300+$1)) $((800+$2)) click 1 #2_1
fi

if [ ${arr[5]}  -eq 1 ]; then
	echo "2_2 press"
	xdotool mousemove $((500+$1)) $((800+$2)) click 1 #2_2
fi

if [ ${arr[6]}  -eq 1 ]; then
	echo "2_3 press"
	xdotool mousemove $((690+$1)) $((800+$2)) click 1 #2_3
fi

if [ ${arr[7]}  -eq 1 ]; then
	echo "2_4 press"
	xdotool mousemove $((900+$1)) $((800+$2)) click 1 #2_4
fi

if [ ${arr[8]}  -eq 1 ]; then
	echo "3_1 press"
	xdotool mousemove $((300+$1)) $((1000+$2)) click 1 #3_1
fi

if [ ${arr[9]}  -eq 1 ]; then
	echo "3_2 press"
	xdotool mousemove $((500+$1)) $((1000+$2)) click 1 #3_2
fi

if [ ${arr[10]}  -eq 1 ]; then
	echo "3_3 press"
	xdotool mousemove $((690+$1)) $((1000+$2)) click 1 #3_3
fi

if [ ${arr[11]}  -eq 1 ]; then
	echo "3_4 press"
	xdotool mousemove $((900+$1)) $((1000+$2)) click 1 #3_4
fi

if [ ${arr[12]}  -eq 1 ]; then
	echo "4_1 press"
	xdotool mousemove $((300+$1)) $((1200+$2)) click 1 #4_1
fi

if [ ${arr[13]}  -eq 1 ]; then
	echo "4_2 press"
	xdotool mousemove $((500+$1)) $((1200+$2)) click 1 #4_2
fi

if [ ${arr[14]}  -eq 1 ]; then
	echo "4_3 press"
	xdotool mousemove $((690+$1)) $((1200+$2)) click 1 #4_3
fi

if [ ${arr[15]}  -eq 1 ]; then
	echo "4_4 press"
	xdotool mousemove $((900+$1)) $((1200+$2)) click 1 #4_4
fi