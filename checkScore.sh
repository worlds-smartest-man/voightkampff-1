get_score()
{
	echo ">>setWindow.sh"
	firefox "https://recaptcha-demo.appspot.com/recaptcha-v3-request-scores.php" &
	sleep 2
	
	n=$RANDOM
	x=$((2780-$n%60))
	y=$((2100-$n%60))
	echo $x $y

	xdotool windowsize `xdotool search --onlyvisible -name firefox` $x $y  # 2280 1600
	xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0

	waitForPuzzle=0
	puzzleLoaded=0
	while [ $puzzleLoaded -eq 0 ] && [ $waitForPuzzle -le 30 ]
	do
		maim -g 900x70+100+1100 scoreLoaded.png
		STR=$(tesseract scoreLoaded.png stdout)	

		SUB='response from our backen'
		if [[ "$STR" == *"$SUB"* ]]; then
		  	puzzleLoaded=1
		  	echo "Score Loaded"
	  	else
			echo "Waiting for score to load"
		fi
		((waitForPuzzle=waitForPuzzle+1))
		sleep 0.5
	done

	maim -g 150x70+320+1330 score.png
	STR=$(tesseract score.png stdout -l eng)
	echo $STR
	xdotool windowsize `xdotool search --onlyvisible -name firefox` 2280 1600
	sleep 1.5
	xdotool mousemove 2240 80 click 1 #Close Firefox properly
	
	# Quick and dirty to get score
	SUB='1'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.1"
		return 1
	fi

	SUB='2'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.2"
	  	return 2
	fi

	SUB='3'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.3"
		return 3
	fi

	SUB='4'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.4"
		return 4
	fi

	SUB='5'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.5"
	  	return 5
	fi

	SUB='6'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.6"
	  	return 6
	fi

	SUB='7'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.7"
	  	return 7
	fi

	SUB='8'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.8"
	  	return 8
	fi

	SUB='9'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "SCORE:0.9"
	  	return 9
	fi
}

