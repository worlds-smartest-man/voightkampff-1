
xdotool windowsize `xdotool search --onlyvisible -name firefox` 2280 1600
xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0
sleep 2
xdotool mousemove 2240 220 click 1   # click on hamburguer menu
sleep 1
xdotool mousemove 1800 970 click 1   # click on preferences
sleep 1
xdotool type zoom
sleep 1
xdotool mousemove 890 620 click 1    # click on zoom menu
sleep 1
xdotool mousemove 820 1030 click 1   # click on 110%
sleep 1
xdotool mousemove 2030 350 click 1   # click on x to 'x' on search textbox
sleep 1
xdotool type proxy
sleep 1
xdotool mousemove 1900 680 click 1   # click on settings button 
sleep 1
xdotool mousemove 270 780 click 1    # click on 'manual proxy configuration'
sleep 1
xdotool mousemove 670 850 click 1    # click on HTTP proxy textbox
sleep 1
xdotool type localhost
sleep 1
xdotool mousemove 1960 850 click 1   # click on port  textbox
sleep 1
xdotool type 3333                    # enter port 3333
sleep 1
xdotool mousemove 530 930 click 1    # click on 'also use this proxy for FTP and HTTPS'
sleep 1
xdotool mousemove 1950 1600 click 1   # click on Ok button