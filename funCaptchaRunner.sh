# --------------------------------------------------------------------------------
# main.sh
#
# Author:  Ozymandias
#          [ef3add5f32de7d98ead2e7bb6dfad933298fc99e6b910fa3678e3ba54e7303df]
# Github:  pirates-of-silicon-hills/VoightKampff
# Date:    November 2019
# License: MIT 
#
#
#     I'm not a comic book villain. Do you seriously
#     think I would explain my master stroke to you 
#     if there were even the slightest possibility 
#     you could affect the outcome?           
#
#     I triggered it 35 minutes ago
#                       				-Adrian Veidt
#
# --------------------------------------------------------------------------------

echo "████████████████"
echo "██          ██  ██" 
echo "██          ██    ██"
echo "██  ████  ██████  ██"
echo "██  ████  ██████████"
echo "██        ██████  ██"
echo "██                ██"
echo "██                ██"
echo "██  ██        ██  ██"
echo "██  ████████████  ██"
echo "██                ██"  
echo "████████████████████"
echo ""
echo "PIRATES OF SILICON HILLS 2020"
echo "MIT License"

printf "\n"

for orbit in {0..1000}
do
	for dns in {0..19}
	do
		echo "Region: " $dns
		./switchDNS.sh $dns
		./setWindow_funCaptcha.sh
		time ./funCaptcha.sh
	done
done

